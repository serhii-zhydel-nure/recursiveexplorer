﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Utils;

namespace BaseThreads
{
  public enum OperationState { ReadyToStart, Working, Finished }

  public class AsyncOperation
  {
    public OperationState State { get; protected set; } = OperationState.ReadyToStart;

    public event Action OnStart;
    public event Action OnFinish;
    protected Thread Thread;

    protected AsyncOperation()
    {}

    public AsyncOperation(Action operationToRunAsync, bool isBackground = true)
    {
      Action work = () =>
      {
        NotifyStartOperation();
        operationToRunAsync();
        NotifyFinishOperation();
      };

      if (isBackground)
      {
        ThreadPool.QueueUserWorkItem(o => work.Invoke());
      }
      else
      {
        Thread = new Thread(new ThreadStart(work));
        Thread.IsBackground = false;
      }
    }

    protected virtual void NotifyStartOperation()
    {
      State = OperationState.Working;
      OnStart.InvokeChecked();
    }

    protected virtual void NotifyFinishOperation()
    {
      State = OperationState.Finished;
      OnFinish.InvokeChecked();
    }

    public void Start()
    {
      Thread.Start();
    }
  }

  public class AsyncOperation<TResult> : AsyncOperation
  {
    public event Action<TResult> OnFinish;

    public bool IsComplete { get { return State == OperationState.Finished; } }

    public TResult Result;

    protected AsyncOperation()
    {}

    public AsyncOperation(Func<TResult> operationToRunAsync, bool isBackground = true)
    {
      Action work = () =>
      {
        NotifyStartOperation();
        Result = operationToRunAsync();
        NotifyFinishOperation();
      };

      if (isBackground)
      {
        ThreadPool.QueueUserWorkItem(o => work.Invoke());
      }
      else
      {
        Thread = new Thread(new ThreadStart(work));
        Thread.IsBackground = false;
      }
    }

    protected override void NotifyFinishOperation()
    {
      State = OperationState.Finished;
      OnFinish.InvokeChecked(Result);
    }
  }
  

  public class AsyncOperationTracked : AsyncOperation
  {
    public float CurrentProgress = 0;
    public event Action<float> OnProgressChanged;

    public AsyncOperationTracked(Func<IEnumerator<float>> operationToRunAsync, bool isBackground = true)
    {
      Action work = () =>
      {
        NotifyStartOperation();
        IEnumerator<float> enumerator = operationToRunAsync();
        do
        {
          float current = enumerator.Current;
          if (current != CurrentProgress)
          {
            CurrentProgress = current;
            OnProgressChanged.InvokeChecked(current);
          }

        } while (enumerator.MoveNext());
        NotifyFinishOperation();
      };

      if (isBackground)
      {
        ThreadPool.QueueUserWorkItem(o => work.Invoke());
      }
      else
      {
        Thread = new Thread(new ThreadStart(work));
        Thread.IsBackground = false;
      }
    }
  }

  public class AsyncOperationTracked<TResult> : AsyncOperation<TResult>
  {
    public float CurrentProgress = 0;
    public event Action<float> OnProgressChanged;

    public AsyncOperationTracked(Func<IEnumerator<Tuple<float, TResult>>> operationToRunAsync, bool isBackground = true)
    {
      Action work = () =>
      {
        NotifyStartOperation();

        IEnumerator<Tuple<float, TResult>> enumerator = operationToRunAsync();
        do
        {
          float current = enumerator.Current.Item1;
          if (current != CurrentProgress)
          {
            CurrentProgress = current;
            OnProgressChanged.InvokeChecked(current);
          }

        } while (enumerator.MoveNext());

        Result = enumerator.Current.Item2;
        NotifyFinishOperation();
      };

      if (isBackground)
      {
        ThreadPool.QueueUserWorkItem(o => work.Invoke());
      }
      else
      {
        Thread = new Thread(new ThreadStart(work));
        Thread.IsBackground = false;
      }
    }
  }
}