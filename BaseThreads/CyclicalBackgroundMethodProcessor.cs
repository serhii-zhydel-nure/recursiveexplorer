﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace BaseThreads
{
	/// <summary>
	/// Always working background thread.
	/// It processes actions which you put in queue.
	/// </summary>
	public class CyclicalBackgroundMethodProcessor : ExceptionHandlerKepper
	{
		private Queue<Action> _actionsToProcessing = new Queue<Action>();

		private AutoResetEvent _getWorkNotitication = new AutoResetEvent(false);

		public CyclicalBackgroundMethodProcessor()
		{
			ThreadPool.QueueUserWorkItem(
				obj =>
				{
					try
					{
						while (true)
						{
							if (_actionsToProcessing.Count > 0)
							{
								lock (((ICollection)_actionsToProcessing).SyncRoot)
								{
									for (int i = 0; i < _actionsToProcessing.Count; i++)
										_actionsToProcessing.Dequeue().Invoke();
								}
							}
							else
								_getWorkNotitication.WaitOne();
						}
					}
					catch (Exception e)
					{
						ExceptionHandler.Invoke(e);
					}
				});
		}

		public void AddMethodToProcessing(Action method)
		{
			lock (((ICollection)_actionsToProcessing).SyncRoot)
			{
				_actionsToProcessing.Enqueue(method);
			}
			_getWorkNotitication.Set();
		}
	}
}