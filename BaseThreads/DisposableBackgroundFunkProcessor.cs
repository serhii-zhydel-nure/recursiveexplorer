﻿using System;
using System.Threading;

namespace BaseThreads
{

	/// <summary>
	/// Works 1 time, processes 1 function in new thread and then puts result into Result-variable.
	/// Thread where is created instance of this class, continues wait till function have completed.
	/// </summary>
	public class DisposableBackgroundFunkProcessor: ExceptionHandlerKepper
	{
		public object Result { get; private set; }

		private AutoResetEvent _doneNotitication = new AutoResetEvent(false);

		public DisposableBackgroundFunkProcessor(Func<object, object> funk, object parameters)
		{
			ThreadPool.QueueUserWorkItem(
				obj =>
					{
						try
						{
							Result = funk(parameters);
							_doneNotitication.Set();
						}
						catch (Exception e)
						{
							ExceptionHandler.Invoke(e);
						}
					});

			_doneNotitication.WaitOne();
		}

	}
}