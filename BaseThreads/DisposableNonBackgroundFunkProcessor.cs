﻿using System;
using System.Threading;

namespace BaseThreads
{
	public class DisposableNonBackgroundFunkProcessor: ExceptionHandlerKepper
	{
		public object Result { get; private set; }

		public DisposableNonBackgroundFunkProcessor(Func<object, object> funk, object parameters)
		{
			Thread processor = new Thread(
				obj =>
					{
						try
						{
							Result = funk(parameters);
						}
						catch (Exception e)
						{
							ExceptionHandler.Invoke(e);
						}
					});
			processor.IsBackground = false;
			processor.Start();
		}
	}
}