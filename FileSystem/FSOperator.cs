﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace FileSystemOperator
{
	/// <summary>
	/// Process all low-level files operations 
	/// </summary>
	public class FSOperator : IFSOperator
	{

		public bool IsItFolder(string path)
		{
			FileAttributes attr = File.GetAttributes(path);

			if (attr.HasFlag(FileAttributes.Directory))
				return true;
			return false;
		}

		public string GetOwner(string path)
		{
			IdentityReference sid = null;
			string owner = null;
			try
			{
				FileSecurity fileSecurity = File.GetAccessControl(path);
				sid = fileSecurity.GetOwner(typeof(SecurityIdentifier));
				NTAccount ntAccount = sid.Translate(typeof(NTAccount)) as NTAccount;
				owner = ntAccount.Value;
				return owner;
			}
			catch (IdentityNotMappedException ex)
			{
				//_userNotificator(String.Format("{0} GetOwner-problem \"{1}\"", ex.Message, path));
				if (sid != null)
					owner = sid.ToString();
				return owner;
			}
		}
		private IdentityReferenceCollection GetUserRoles()
		{
			return WindowsIdentity.GetCurrent().Groups;
		}
		public List<FileSystemRights> GetUserRights(string path)
		{
			List<FileSystemRights> result = new List<FileSystemRights>();

			foreach (System.Security.AccessControl.FileSystemAccessRule fileRule in
					File.GetAccessControl(path).GetAccessRules(true, true, typeof(NTAccount)))
			{
				foreach (IdentityReference userIdentity in GetUserRoles())
				{
					NTAccount userAccount = (NTAccount)userIdentity.Translate(typeof(NTAccount));
					NTAccount fileAccount = (NTAccount)fileRule.IdentityReference.Translate(typeof(NTAccount));

					if (userAccount == fileAccount)
					{
						result.Add(fileRule.FileSystemRights);
					}
				}
			}
			return result;
		}

		public DriveInfo[] GetDrives()
		{
			return DriveInfo.GetDrives();
		}


		public bool IsThereFileWithSimilarName(string filePath, string folderPath)
		{
			FileInfo ethalonFile = GetFile(filePath);

			foreach (FileInfo checkedFile in GetFolderFiles(folderPath))
			{
				if (checkedFile.Name == ethalonFile.Name)
					return true;
			}

			return false;
		}

		public DirectoryInfo GetFolder(string folderPath)
		{
			return new DirectoryInfo(folderPath);
		}
		public void MoveFolder(string folderPath, string destinationPath, Func<string, bool> rewriteConfirmator)
		{
			CloseAllFilles(folderPath, true);
			DirectoryInfo dir = new DirectoryInfo(folderPath);
			dir.MoveTo(destinationPath);
		}
		public void CopyFolder(string folderPath, string destinationPath, Func<string, bool> rewriteConfirmator)
		{
			CloseAllFilles(folderPath, true);
			// Get the subdirectories for the specified directory.
			DirectoryInfo folderSource = new DirectoryInfo(folderPath);
			string dirName = folderSource.Name;
			DirectoryInfo folderDestination =
					new DirectoryInfo(destinationPath + Path.DirectorySeparatorChar + dirName);

			if (!folderDestination.Exists)
				folderDestination.Create();

			// Get the files in the directory and copy them to the new location.
			foreach (FileInfo file in folderSource.GetFiles())
			{
				string destinationFileName = Path.Combine(folderDestination.FullName, file.Name);

				if (IsThereFileWithSimilarName(file.FullName, folderDestination.FullName))
				{
					bool rewriteChoice = rewriteConfirmator.Invoke(Path.GetFileName(file.FullName));
					CopyFile(file.FullName, destinationFileName, rewriteChoice);
				}
				else
				{
					CopyFile(file.FullName, destinationFileName, true);
				}
			}

			DirectoryInfo[] subFolders = folderSource.GetDirectories();

			// If copying subdirectories, copy them and their contents to new location.
			foreach (DirectoryInfo subdir in subFolders)
			{
				string temppath = Path.Combine(destinationPath, subdir.Name);
				CopyFolder(subdir.FullName, temppath, rewriteConfirmator);
			}
		}
		public void DeleteFolder(string folderPath)
		{
			CloseAllFilles(folderPath, false);

			DirectoryInfo folder = new DirectoryInfo(folderPath);
			foreach (FileInfo file in folder.GetFiles())
			{
				file.Delete();
			}

			foreach (DirectoryInfo subFolder in folder.GetDirectories())
			{
				DeleteFolder(subFolder.FullName);
			}

			Directory.Delete(folderPath);
		}

		public void CloseAllFilles(string folderPath, bool recursive)
		{
			DirectoryInfo folder = new DirectoryInfo(folderPath);
			foreach (FileInfo file in folder.GetFiles())
			{
				StreamReader reader = new StreamReader(file.FullName);
				reader.Close();
			}

			if (recursive)
				foreach (DirectoryInfo subFolder in folder.GetDirectories())
				{
					CloseAllFilles(subFolder.FullName, recursive);
				}
		}

		public DirectoryInfo[] GetFolderSubFolders(string folderPath)
		{
			return new DirectoryInfo(folderPath).GetDirectories();
		}
		public FileInfo[] GetFolderFiles(string folderPath)
		{
			return new DirectoryInfo(folderPath).GetFiles();
		}
		public long GetFolderSize(string folderPath, bool recursive)
		{
			long resultSize = 0;

			// get the sizeof all files in the current directory
			foreach (string filePath in Directory.GetFiles(folderPath))
			{
				resultSize += new FileInfo(filePath).Length;
			}

			if (recursive)
				// get the size of all files in all subdirectories
				foreach (string _folderPath in Directory.GetDirectories(folderPath))
				{
					resultSize += GetFolderSize(_folderPath, recursive);
				}

			return resultSize;
		}


		public FileInfo GetFile(string filePath)
		{
			return new FileInfo(filePath);
		}
		public void MoveFile(string filePath, string destinationPath, bool rewriteOption)
		{
			FileInfo fi = new FileInfo(filePath);

			if (rewriteOption == false)
				return;

			new FileInfo(destinationPath).Delete();
			fi.MoveTo(destinationPath);
		}
		public void CopyFile(string filePath, string destinationPath, bool rewriteOption)
		{
			FileInfo fi = new FileInfo(filePath);

			if (rewriteOption == false)
				return;

			fi.CopyTo(destinationPath, rewriteOption);
		}
		public void DeleteFile(string filePath)
		{
			FileInfo file = new FileInfo(filePath);
			file.Delete();
		}

	}
}