﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using BaseThreads;

namespace FileSystemOperator
{
	/// <summary>
	/// Thread-wrap of FSOperator
	/// Wraped-methods of this class work in their own threads
	/// </summary>
	public class FSOperatorDisposable : IFSOperator
	{
		private FSOperator _fsOperator = new FSOperator();

		#region Stuff
		public object GetOwnerWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			return _fsOperator.GetOwner(parameter);
		}
		public string GetOwner(string path)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetOwnerWrap, path);
			return funkProcessor.Result as string;
		}

		public object GetUserRightsWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			return _fsOperator.GetUserRights(parameter);
		}
		public List<FileSystemRights> GetUserRights(string path)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetUserRightsWrap, path);
			return funkProcessor.Result as List<FileSystemRights>;
		}


		public object GetDrivesWrap(object paramWrap)
		{
			return _fsOperator.GetDrives();
		}
		public DriveInfo[] GetDrives()
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetDrivesWrap, null);
			return funkProcessor.Result as DriveInfo[];
		}
		#endregion

		#region Folders
		public object IsItFolderWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			return _fsOperator.IsItFolder(parameter);
		}
		public bool IsItFolder(string path)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(IsItFolderWrap, path);
			return (bool)funkProcessor.Result;
		}

		public object GetFolderWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			return _fsOperator.GetFolder(parameter);
		}
		public DirectoryInfo GetFolder(string folderPath)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetFolderWrap, folderPath);
			return funkProcessor.Result as DirectoryInfo;
		}

		public object CopyFolderWrap(object paramWrap)
		{
			List<object> parameter = paramWrap as List<object>;
			_fsOperator.CopyFolder(parameter[0] as string, parameter[1] as string, parameter[2] as Func<string, bool>);
			return null;
		}
		public void CopyFolder(string folderPath, string destinationPath, Func<string, bool> rewriteConfirmator)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(CopyFolderWrap,
					new List<object>() { folderPath, destinationPath, rewriteConfirmator });
		}

		public object GetFolderSubFoldersWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			return _fsOperator.GetFolderSubFolders(parameter);
		}
		public DirectoryInfo[] GetFolderSubFolders(string folderPath)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetFolderSubFoldersWrap, folderPath);
			return funkProcessor.Result as DirectoryInfo[];
		}

		public object IsThereFileWithSimilarNameWrap(object paramWrap)
		{
			List<object> parameter = paramWrap as List<object>;
			return _fsOperator.IsThereFileWithSimilarName(parameter[0] as string, parameter[1] as string);
		}
		public bool IsThereFileWithSimilarName(string filePath, string folderPath)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(IsThereFileWithSimilarNameWrap, new List<object>() { filePath, folderPath });
			return (bool)funkProcessor.Result;
		}

		public object GetFolderFilesWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			return _fsOperator.GetFolderFiles(parameter);
		}
		public FileInfo[] GetFolderFiles(string folderPath)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetFolderFilesWrap, folderPath);
			return funkProcessor.Result as FileInfo[];
		}

		public object MoveFolderWrap(object paramWrap)
		{
			List<object> parameter = paramWrap as List<object>;
			_fsOperator.MoveFolder(parameter[0] as string, parameter[1] as string, parameter[2] as Func<string, bool>);
			return null;
		}
		public void MoveFolder(string folderPath, string destinationPath, Func<string, bool> rewriteConfirmator)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(MoveFolderWrap,
				new List<object>() { folderPath, destinationPath, rewriteConfirmator });
		}

		public object DeleteFolderWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			_fsOperator.DeleteFolder(parameter);
			return null;
		}
		public void DeleteFolder(string folderPath)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(DeleteFolderWrap, folderPath);
		}

		public object GetFolderSizeWrap(object paramWrap)
		{
			List<object> parameter = paramWrap as List<object>;
			return _fsOperator.GetFolderSize(parameter[0] as string, (bool)parameter[1]);
		}
		public long GetFolderSize(string folderPath, bool recursive)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetFolderSizeWrap, new List<object>() { folderPath, recursive });
			return (long)funkProcessor.Result;
		}

		public object CloseAllFillesWrap(object paramWrap)
		{
			List<object> parameter = paramWrap as List<object>;
			_fsOperator.CloseAllFilles(parameter[0] as string, (bool)parameter[1]);
			return null;
		}
		public void CloseAllFilles(string folderPath, bool recursive)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(CloseAllFillesWrap, new List<object>() { folderPath, recursive });
		}

		#endregion

		#region Files
		public object GetFileWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			return _fsOperator.GetFile(parameter);
		}
		public FileInfo GetFile(string filePath)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(GetFileWrap, filePath);
			return funkProcessor.Result as FileInfo;
		}

		public object CopyFileWrap(object paramWrap)
		{
			List<object> parameter = paramWrap as List<object>;
			_fsOperator.CopyFile(parameter[0] as string, parameter[1] as string, (bool)parameter[2]);
			return null;
		}
		public void CopyFile(string filePath, string destinationPath, bool rewriteOption)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(CopyFileWrap,
					new List<object>() { filePath, destinationPath, rewriteOption });
		}

		public object MoveFileWrap(object paramWrap)
		{
			List<object> parameter = paramWrap as List<object>;
			_fsOperator.MoveFile(parameter[0] as string, parameter[1] as string, (bool)parameter[2]);
			return null;
		}
		public void MoveFile(string filePath, string destinationPath, bool rewriteOption)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(MoveFileWrap,
				new List<object>() { filePath, destinationPath, rewriteOption });
		}

		public object DeleteFileWrap(object paramWrap)
		{
			string parameter = paramWrap as string;
			_fsOperator.DeleteFile(parameter);
			return null;
		}
		public void DeleteFile(string filePath)
		{
			DisposableBackgroundFunkProcessor funkProcessor = new DisposableBackgroundFunkProcessor(DeleteFileWrap, filePath);
		}

		#endregion
	}
}