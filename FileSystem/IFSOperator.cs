﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;

namespace FileSystemOperator
{
    public interface IFSOperator
    {
        bool IsItFolder(string path);

        string GetOwner(string path);
        List<FileSystemRights> GetUserRights(string path);


        DriveInfo[] GetDrives();


        DirectoryInfo GetFolder(string folderPath);
        void MoveFolder(string folderPath, string destinationPath, Func<string, bool> rewriteConfirmator);
        void CopyFolder(string folderPath, string destinationPath, Func<string, bool> rewriteConfirmator);
        void DeleteFolder(string folderPath);


        DirectoryInfo[] GetFolderSubFolders(string folderPath);
        FileInfo[] GetFolderFiles(string folderPath);
        bool IsThereFileWithSimilarName(string filePath, string folderPath);
        long GetFolderSize(string folderPath, bool recursive);
        void CloseAllFilles(string folderPath, bool recursive);

        FileInfo GetFile(string filePath);
        void MoveFile(string filePath, string destinationPath, bool rewriteOption);
        void CopyFile(string filePath, string destinationPath, bool rewriteOption);
        void DeleteFile(string filePath);
    }
}