﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Forms;
using BaseThreads;
using FileSystemOperator;
using RecursiveExplorer.View;
using RecursiveExplorer.ViewModel;

namespace RecursiveExplorer
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			try
			{
				ExceptionHandlerKepper.ExceptionHandler = HandleException;

				UIView window = new UIView();

				//info-loading threads
				//if change instance type on FSOperator, then it'll work in thread where it's invoked 
				IFSOperator fileOperator = new FSOperatorDisposable();

				//thread view-builder (handle window's requests and load information into the window)
				ViewHandler viewHandler = new ViewHandler(window, fileOperator);

				//thread main
				Application.Run(window);
			}
			catch (Exception e)
			{
				HandleException(e);
			}
		}

		private static void HandleException(Exception e)
		{
			string reportPath = Path.Combine(Environment.CurrentDirectory, "Exception Reports.txt");

			using (StreamWriter writer = new StreamWriter(reportPath, true))
			{
				writer.WriteLine(e);
			}

			string message = String.Format("Program encountered with error: {0}" +
																		 "\"{1}\".{0}" +
																		 "Program saved report in the file: {0}" +
																		 "\"{2}\"", Environment.NewLine, e.Message, reportPath);

			DialogResult dialogResult = MessageBox.Show(message, "Error", MessageBoxButtons.OK);

			Application.Restart();
		}

	}
}
