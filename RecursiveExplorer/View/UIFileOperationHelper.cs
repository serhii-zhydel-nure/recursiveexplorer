﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RecursiveExplorer.View
{
	public enum ViewState
	{
		BrowsingFiles, SelectingFolderForPaste, DoingFileOperation
	}

	/// <summary>
	/// Functions:
	/// 1 - file-operation form-elements controlling of "enable" or "visible" state
	/// 2 - selected items store
	/// </summary>
	public class UIFileOperationHelper
	{
		public List<string> SelectedItemList
		{
			get { return _selectedItemList; }
			set
			{
				_selectedItemList = value;
				SelectListUpdated(_window.PathString);
			}
		}

		private event Action<string> SelectListUpdated;

		private List<string> _selectedItemList;

		public ViewState State
		{
			get
			{
				return _state;
			}
			private set
			{
				_state = value;
				if (_state == ViewState.DoingFileOperation)
				{
					_progressBarOperation.Visible = true;
					_labelTotalProgress.Visible = true;
				}
				else
				{
					_progressBarOperation.Visible = false;
					_labelTotalProgress.Visible = false;
				}
			}
		}

		private ViewState _state = ViewState.BrowsingFiles;

		private string _sourceFolder;
		private bool IsNowInSourceFolder
		{
			get { return _window.PathString == _sourceFolder; }
		}

		private UIView _window;

		private Button _cutButton;
		private Button _coppyButton;
		private Button _pasteButton;
		private Button _cancelButton;
		private Button _deleteButton;

		private Label _cancelLabel;
		private Label _pasteTip;
		private Label _sourceFolderTip;
		private Label _destinationFolderTip;

		private ProgressBar _progressBarOperation;
		private Label _labelTotalProgress;

		public UIFileOperationHelper(UIView window, Button cut, Button coppy, Button paste, Button cancel, Button delete,
				Label cancelLabel, Label pasteTip, Label sourceFolderTip, Label destinationFolderTip,
				ProgressBar progressBarOperation, Label labelTotalProgress)
		{
			_window = window;
			_selectedItemList = new List<string>();
			SelectListUpdated += UpdateUIElementsState;

			_cutButton = cut;
			_coppyButton = coppy;
			_pasteButton = paste;
			_cancelButton = cancel;
			_deleteButton = delete;

			_cancelLabel = cancelLabel;
			_pasteTip = pasteTip;
			_sourceFolderTip = sourceFolderTip;
			_destinationFolderTip = destinationFolderTip;

			_progressBarOperation = progressBarOperation;
			_labelTotalProgress = labelTotalProgress;

			_window.CoppyButtonPressed += CutOrCopyPressed;
			_window.CutButtonPressed += CutOrCopyPressed;
			_window.PasteButtonPressed += PastePressed;
			_window.CancelButtonPressed += FinishOperation;
			_window.DeleteButtonPressed += DeletePressed;

			_window.PathChanged += UpdateUIElementsState;

			_window.ProgressBarFilled += FinishOperation;
		}

		private void CutOrCopyPressed(List<string> selectedItems)
		{
			_sourceFolder = _window.PathString;
			State = ViewState.SelectingFolderForPaste;
			UpdateUIElementsState(_sourceFolder);
		}


		private void PastePressed()
		{
			State = ViewState.DoingFileOperation;
			UpdateUIElementsState(_sourceFolder);
		}

		private void CancelPressed()
		{
			State = ViewState.BrowsingFiles;
			UpdateUIElementsState(_sourceFolder);
		}

		private void DeletePressed(List<string> selectedItems)
		{
			_sourceFolder = _window.PathString;
			State = ViewState.DoingFileOperation;
			UpdateUIElementsState(_sourceFolder);
		}


		private void UpdateUIElementsState(string path)
		{
			if (State == ViewState.BrowsingFiles)
			{
				if (_selectedItemList.Count > 0)
				{
					_cutButton.Enabled = true;
					_coppyButton.Enabled = true;
					_deleteButton.Enabled = true;
				}
				else
				{
					_cutButton.Enabled = false;
					_coppyButton.Enabled = false;
					_deleteButton.Enabled = false;
				}

				_pasteButton.Enabled = false;
				_cancelButton.Visible = false;

				_cancelLabel.Visible = false;
				_pasteTip.Visible = false;
				_sourceFolderTip.Visible = false;
				_destinationFolderTip.Visible = false;
			}

			if (State == ViewState.SelectingFolderForPaste)
			{
				_cutButton.Enabled = false;
				_coppyButton.Enabled = false;
				_deleteButton.Enabled = false;

				_cancelButton.Visible = true;
				_cancelLabel.Visible = true;

				_pasteButton.Enabled = !IsNowInSourceFolder;
				_pasteTip.Visible = true;
				_sourceFolderTip.Visible = IsNowInSourceFolder;
				_destinationFolderTip.Visible = !_sourceFolderTip.Visible;
			}

			if (State == ViewState.DoingFileOperation)
			{
				_cutButton.Enabled = false;
				_coppyButton.Enabled = false;
				_deleteButton.Enabled = false;

				_pasteButton.Enabled = false;

				_cancelButton.Visible = true;
				_cancelLabel.Visible = true;

				_pasteTip.Visible = false;
				_sourceFolderTip.Visible = false;
				_destinationFolderTip.Visible = false;
			}
		}


		private void FinishOperation()
		{
			State = ViewState.BrowsingFiles;

			UpdateUIElementsState(_sourceFolder);
		}
	}
}