﻿namespace RecursiveExplorer.View
{
    partial class UIView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tree = new System.Windows.Forms.TreeView();
            this._panelNavigation = new System.Windows.Forms.Panel();
            this.buttonGo = new System.Windows.Forms.Button();
            this._buttonUp = new System.Windows.Forms.Button();
            this._buttonBack = new System.Windows.Forms.Button();
            this._buttonForward = new System.Windows.Forms.Button();
            this._pathTextBox = new System.Windows.Forms.TextBox();
            this._labelPath = new System.Windows.Forms.Label();
            this._panelProperties = new System.Windows.Forms.Panel();
            this._labelTotalProgress = new System.Windows.Forms.Label();
            this._progressBarOperation = new System.Windows.Forms.ProgressBar();
            this._labelDelete = new System.Windows.Forms.Label();
            this._buttonDelete = new System.Windows.Forms.Button();
            this._labelDestinationFolderTip = new System.Windows.Forms.Label();
            this._labelCancel = new System.Windows.Forms.Label();
            this._labelPaste = new System.Windows.Forms.Label();
            this._labelCopy = new System.Windows.Forms.Label();
            this._labelCut = new System.Windows.Forms.Label();
            this._buttonCancel = new System.Windows.Forms.Button();
            this._labelSourceFolderTip = new System.Windows.Forms.Label();
            this._labelPasteTip = new System.Windows.Forms.Label();
            this._checkBoxBustSubfolders = new System.Windows.Forms.CheckBox();
            this._buttonSaveXmlReport = new System.Windows.Forms.Button();
            this._buttonPaste = new System.Windows.Forms.Button();
            this._buttonCut = new System.Windows.Forms.Button();
            this._buttonCopy = new System.Windows.Forms.Button();
            this._containerListView = new System.Windows.Forms.ListView();
            this._panelNavigation.SuspendLayout();
            this._panelProperties.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tree
            // 
            this._tree.Dock = System.Windows.Forms.DockStyle.Left;
            this._tree.Indent = 15;
            this._tree.Location = new System.Drawing.Point(0, 0);
            this._tree.Name = "_tree";
            this._tree.Size = new System.Drawing.Size(206, 556);
            this._tree.TabIndex = 0;
            this._tree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tree_NodeMouseClick);
            // 
            // _panelNavigation
            // 
            this._panelNavigation.Controls.Add(this.buttonGo);
            this._panelNavigation.Controls.Add(this._buttonUp);
            this._panelNavigation.Controls.Add(this._buttonBack);
            this._panelNavigation.Controls.Add(this._buttonForward);
            this._panelNavigation.Controls.Add(this._pathTextBox);
            this._panelNavigation.Controls.Add(this._labelPath);
            this._panelNavigation.Dock = System.Windows.Forms.DockStyle.Top;
            this._panelNavigation.Location = new System.Drawing.Point(206, 0);
            this._panelNavigation.Name = "_panelNavigation";
            this._panelNavigation.Size = new System.Drawing.Size(781, 43);
            this._panelNavigation.TabIndex = 2;
            // 
            // buttonGo
            // 
            this.buttonGo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonGo.BackgroundImage = global::RecursiveExplorer.Properties.Resources.go;
            this.buttonGo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonGo.Location = new System.Drawing.Point(736, 8);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(33, 27);
            this.buttonGo.TabIndex = 6;
            this.buttonGo.UseVisualStyleBackColor = false;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // _buttonUp
            // 
            this._buttonUp.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonUp.BackgroundImage = global::RecursiveExplorer.Properties.Resources.up;
            this._buttonUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonUp.Enabled = false;
            this._buttonUp.Location = new System.Drawing.Point(117, 3);
            this._buttonUp.Name = "_buttonUp";
            this._buttonUp.Size = new System.Drawing.Size(37, 36);
            this._buttonUp.TabIndex = 5;
            this._buttonUp.UseVisualStyleBackColor = false;
            this._buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // _buttonBack
            // 
            this._buttonBack.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonBack.BackgroundImage = global::RecursiveExplorer.Properties.Resources.back;
            this._buttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonBack.Enabled = false;
            this._buttonBack.Location = new System.Drawing.Point(20, 3);
            this._buttonBack.Name = "_buttonBack";
            this._buttonBack.Size = new System.Drawing.Size(37, 36);
            this._buttonBack.TabIndex = 4;
            this._buttonBack.UseVisualStyleBackColor = false;
            this._buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // _buttonForward
            // 
            this._buttonForward.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonForward.BackgroundImage = global::RecursiveExplorer.Properties.Resources.forward;
            this._buttonForward.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonForward.Enabled = false;
            this._buttonForward.Location = new System.Drawing.Point(63, 3);
            this._buttonForward.Name = "_buttonForward";
            this._buttonForward.Size = new System.Drawing.Size(37, 36);
            this._buttonForward.TabIndex = 3;
            this._buttonForward.UseVisualStyleBackColor = false;
            this._buttonForward.Click += new System.EventHandler(this.buttonForward_Click);
            // 
            // _pathTextBox
            // 
            this._pathTextBox.Location = new System.Drawing.Point(229, 12);
            this._pathTextBox.Name = "_pathTextBox";
            this._pathTextBox.Size = new System.Drawing.Size(501, 20);
            this._pathTextBox.TabIndex = 1;
            // 
            // _labelPath
            // 
            this._labelPath.AutoSize = true;
            this._labelPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this._labelPath.Location = new System.Drawing.Point(188, 13);
            this._labelPath.Name = "_labelPath";
            this._labelPath.Size = new System.Drawing.Size(35, 15);
            this._labelPath.TabIndex = 0;
            this._labelPath.Text = "Path:";
            // 
            // _panelProperties
            // 
            this._panelProperties.Controls.Add(this._labelTotalProgress);
            this._panelProperties.Controls.Add(this._progressBarOperation);
            this._panelProperties.Controls.Add(this._labelDelete);
            this._panelProperties.Controls.Add(this._buttonDelete);
            this._panelProperties.Controls.Add(this._labelDestinationFolderTip);
            this._panelProperties.Controls.Add(this._labelCancel);
            this._panelProperties.Controls.Add(this._labelPaste);
            this._panelProperties.Controls.Add(this._labelCopy);
            this._panelProperties.Controls.Add(this._labelCut);
            this._panelProperties.Controls.Add(this._buttonCancel);
            this._panelProperties.Controls.Add(this._labelSourceFolderTip);
            this._panelProperties.Controls.Add(this._labelPasteTip);
            this._panelProperties.Controls.Add(this._checkBoxBustSubfolders);
            this._panelProperties.Controls.Add(this._buttonSaveXmlReport);
            this._panelProperties.Controls.Add(this._buttonPaste);
            this._panelProperties.Controls.Add(this._buttonCut);
            this._panelProperties.Controls.Add(this._buttonCopy);
            this._panelProperties.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._panelProperties.Location = new System.Drawing.Point(206, 493);
            this._panelProperties.Name = "_panelProperties";
            this._panelProperties.Size = new System.Drawing.Size(781, 63);
            this._panelProperties.TabIndex = 3;
            // 
            // _labelTotalProgress
            // 
            this._labelTotalProgress.AutoSize = true;
            this._labelTotalProgress.Location = new System.Drawing.Point(201, 43);
            this._labelTotalProgress.Name = "_labelTotalProgress";
            this._labelTotalProgress.Size = new System.Drawing.Size(95, 13);
            this._labelTotalProgress.TabIndex = 21;
            this._labelTotalProgress.Text = "Total Progress: 0%";
            this._labelTotalProgress.Visible = false;
            // 
            // _progressBarOperation
            // 
            this._progressBarOperation.Location = new System.Drawing.Point(172, 19);
            this._progressBarOperation.Name = "_progressBarOperation";
            this._progressBarOperation.Size = new System.Drawing.Size(164, 23);
            this._progressBarOperation.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this._progressBarOperation.TabIndex = 5;
            this._progressBarOperation.Visible = false;
            // 
            // _labelDelete
            // 
            this._labelDelete.AutoSize = true;
            this._labelDelete.Location = new System.Drawing.Point(437, 44);
            this._labelDelete.Name = "_labelDelete";
            this._labelDelete.Size = new System.Drawing.Size(38, 13);
            this._labelDelete.TabIndex = 20;
            this._labelDelete.Text = "Delete";
            // 
            // _buttonDelete
            // 
            this._buttonDelete.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonDelete.BackgroundImage = global::RecursiveExplorer.Properties.Resources.delete;
            this._buttonDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonDelete.Location = new System.Drawing.Point(437, 6);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(37, 36);
            this._buttonDelete.TabIndex = 19;
            this._buttonDelete.UseVisualStyleBackColor = false;
            this._buttonDelete.Click += new System.EventHandler(this._buttonDelete_Click);
            // 
            // _labelDestinationFolderTip
            // 
            this._labelDestinationFolderTip.AutoSize = true;
            this._labelDestinationFolderTip.Location = new System.Drawing.Point(201, 35);
            this._labelDestinationFolderTip.Name = "_labelDestinationFolderTip";
            this._labelDestinationFolderTip.Size = new System.Drawing.Size(104, 13);
            this._labelDestinationFolderTip.TabIndex = 18;
            this._labelDestinationFolderTip.Text = "(you can paste here)";
            this._labelDestinationFolderTip.Visible = false;
            // 
            // _labelCancel
            // 
            this._labelCancel.AutoSize = true;
            this._labelCancel.Location = new System.Drawing.Point(363, 44);
            this._labelCancel.Name = "_labelCancel";
            this._labelCancel.Size = new System.Drawing.Size(40, 13);
            this._labelCancel.TabIndex = 17;
            this._labelCancel.Text = "Cancel";
            this._labelCancel.Visible = false;
            // 
            // _labelPaste
            // 
            this._labelPaste.AutoSize = true;
            this._labelPaste.Location = new System.Drawing.Point(117, 44);
            this._labelPaste.Name = "_labelPaste";
            this._labelPaste.Size = new System.Drawing.Size(34, 13);
            this._labelPaste.TabIndex = 16;
            this._labelPaste.Text = "Paste";
            // 
            // _labelCopy
            // 
            this._labelCopy.AutoSize = true;
            this._labelCopy.Location = new System.Drawing.Point(65, 44);
            this._labelCopy.Name = "_labelCopy";
            this._labelCopy.Size = new System.Drawing.Size(31, 13);
            this._labelCopy.TabIndex = 15;
            this._labelCopy.Text = "Copy";
            // 
            // _labelCut
            // 
            this._labelCut.AutoSize = true;
            this._labelCut.Location = new System.Drawing.Point(27, 44);
            this._labelCut.Name = "_labelCut";
            this._labelCut.Size = new System.Drawing.Size(34, 13);
            this._labelCut.TabIndex = 14;
            this._labelCut.Text = "Move";
            // 
            // _buttonCancel
            // 
            this._buttonCancel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonCancel.BackgroundImage = global::RecursiveExplorer.Properties.Resources.cancel;
            this._buttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonCancel.Location = new System.Drawing.Point(363, 6);
            this._buttonCancel.Name = "_buttonCancel";
            this._buttonCancel.Size = new System.Drawing.Size(37, 36);
            this._buttonCancel.TabIndex = 13;
            this._buttonCancel.UseVisualStyleBackColor = false;
            this._buttonCancel.Visible = false;
            this._buttonCancel.Click += new System.EventHandler(this._buttonCancel_Click);
            // 
            // _labelSourceFolderTip
            // 
            this._labelSourceFolderTip.Location = new System.Drawing.Point(188, 27);
            this._labelSourceFolderTip.Name = "_labelSourceFolderTip";
            this._labelSourceFolderTip.Size = new System.Drawing.Size(117, 29);
            this._labelSourceFolderTip.TabIndex = 12;
            this._labelSourceFolderTip.Text = "(you cannot paste in the source folder)";
            this._labelSourceFolderTip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._labelSourceFolderTip.Visible = false;
            // 
            // _labelPasteTip
            // 
            this._labelPasteTip.AutoSize = true;
            this._labelPasteTip.Location = new System.Drawing.Point(169, 14);
            this._labelPasteTip.Name = "_labelPasteTip";
            this._labelPasteTip.Size = new System.Drawing.Size(167, 13);
            this._labelPasteTip.TabIndex = 11;
            this._labelPasteTip.Text = "Go to the folder you want to paste";
            this._labelPasteTip.Visible = false;
            // 
            // _checkBoxBustSubfolders
            // 
            this._checkBoxBustSubfolders.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._checkBoxBustSubfolders.Location = new System.Drawing.Point(530, 14);
            this._checkBoxBustSubfolders.Name = "_checkBoxBustSubfolders";
            this._checkBoxBustSubfolders.Size = new System.Drawing.Size(122, 38);
            this._checkBoxBustSubfolders.TabIndex = 10;
            this._checkBoxBustSubfolders.Text = "Bust Subfolders (Recursive saving)";
            this._checkBoxBustSubfolders.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._checkBoxBustSubfolders.UseVisualStyleBackColor = true;
            // 
            // _buttonSaveXmlReport
            // 
            this._buttonSaveXmlReport.Location = new System.Drawing.Point(668, 14);
            this._buttonSaveXmlReport.Name = "_buttonSaveXmlReport";
            this._buttonSaveXmlReport.Size = new System.Drawing.Size(101, 39);
            this._buttonSaveXmlReport.TabIndex = 9;
            this._buttonSaveXmlReport.Text = "Save XML Report On This Folder";
            this._buttonSaveXmlReport.UseVisualStyleBackColor = true;
            this._buttonSaveXmlReport.Click += new System.EventHandler(this.buttonSaveXmlReport_Click);
            // 
            // _buttonPaste
            // 
            this._buttonPaste.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonPaste.BackgroundImage = global::RecursiveExplorer.Properties.Resources.paste;
            this._buttonPaste.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonPaste.Enabled = false;
            this._buttonPaste.Location = new System.Drawing.Point(117, 6);
            this._buttonPaste.Name = "_buttonPaste";
            this._buttonPaste.Size = new System.Drawing.Size(37, 36);
            this._buttonPaste.TabIndex = 8;
            this._buttonPaste.UseVisualStyleBackColor = false;
            this._buttonPaste.Click += new System.EventHandler(this._buttonPaste_Click);
            // 
            // _buttonCut
            // 
            this._buttonCut.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonCut.BackgroundImage = global::RecursiveExplorer.Properties.Resources.cut;
            this._buttonCut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonCut.Enabled = false;
            this._buttonCut.Location = new System.Drawing.Point(20, 6);
            this._buttonCut.Name = "_buttonCut";
            this._buttonCut.Size = new System.Drawing.Size(37, 36);
            this._buttonCut.TabIndex = 7;
            this._buttonCut.UseVisualStyleBackColor = false;
            this._buttonCut.Click += new System.EventHandler(this._buttonCut_Click);
            // 
            // _buttonCopy
            // 
            this._buttonCopy.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._buttonCopy.BackgroundImage = global::RecursiveExplorer.Properties.Resources.copy;
            this._buttonCopy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._buttonCopy.Enabled = false;
            this._buttonCopy.Location = new System.Drawing.Point(63, 6);
            this._buttonCopy.Name = "_buttonCopy";
            this._buttonCopy.Size = new System.Drawing.Size(37, 36);
            this._buttonCopy.TabIndex = 6;
            this._buttonCopy.UseVisualStyleBackColor = false;
            this._buttonCopy.Click += new System.EventHandler(this._buttonCopy_Click);
            // 
            // _containerListView
            // 
            this._containerListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._containerListView.Location = new System.Drawing.Point(206, 43);
            this._containerListView.Name = "_containerListView";
            this._containerListView.Size = new System.Drawing.Size(781, 450);
            this._containerListView.TabIndex = 4;
            this._containerListView.UseCompatibleStateImageBehavior = false;
            this._containerListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this._containerListView_ItemSelectionChanged);
            this._containerListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.contentsListView_MouseDoubleClick);
            // 
            // UIView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(987, 556);
            this.Controls.Add(this._containerListView);
            this.Controls.Add(this._panelProperties);
            this.Controls.Add(this._panelNavigation);
            this.Controls.Add(this._tree);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "UIView";
            this.Text = "Explorer";
            this._panelNavigation.ResumeLayout(false);
            this._panelNavigation.PerformLayout();
            this._panelProperties.ResumeLayout(false);
            this._panelProperties.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView _tree;
        private System.Windows.Forms.Panel _panelNavigation;
        private System.Windows.Forms.Panel _panelProperties;
        private System.Windows.Forms.TextBox _pathTextBox;
        private System.Windows.Forms.Label _labelPath;
        private System.Windows.Forms.Button _buttonUp;
        private System.Windows.Forms.Button _buttonBack;
        private System.Windows.Forms.Button _buttonForward;
        private System.Windows.Forms.Button _buttonPaste;
        private System.Windows.Forms.Button _buttonCut;
        private System.Windows.Forms.Button _buttonCopy;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.CheckBox _checkBoxBustSubfolders;
        private System.Windows.Forms.Button _buttonSaveXmlReport;
        private System.Windows.Forms.Label _labelSourceFolderTip;
        private System.Windows.Forms.Label _labelPasteTip;
        private System.Windows.Forms.ListView _containerListView;
        private System.Windows.Forms.Button _buttonCancel;
        private System.Windows.Forms.Label _labelCancel;
        private System.Windows.Forms.Label _labelPaste;
        private System.Windows.Forms.Label _labelCopy;
        private System.Windows.Forms.Label _labelCut;
        private System.Windows.Forms.Label _labelDestinationFolderTip;
        private System.Windows.Forms.Label _labelDelete;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.ProgressBar _progressBarOperation;
        private System.Windows.Forms.Label _labelTotalProgress;
    }
}