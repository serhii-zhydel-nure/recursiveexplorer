﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace RecursiveExplorer.View
{
	public delegate void NodeDelegate(TreeNode node);
	public delegate void ContentsDelegate(string selectedItemName);
	public delegate void ItemOperatinDelegate(List<string> selectedItems);
	public delegate void SaveReport(string folderName, string fileToSave, bool bustSubfolders);


	public partial class UIView : Form
	{
		public SynchronizationContext WindowContext;

		public string PathString
		{
			get { return _pathTextBox.Text; }
			set
			{
				if (_pathTextBox.Text != value)
				{
					_pathTextBox.Text = value;
					if (PathChanged != null)
						PathChanged.Invoke(_pathTextBox.Text);
				}
			}
		}

		public SaveReport SaveReportButtonPressed;

		public int ProgressOperation
		{
			get { return _progressBarOperation.Value; }
			set
			{
				_progressBarOperation.Value = value;

				_labelTotalProgress.Text = String.Format("Total Progress: {0}%", value);

				if (value >= 100)
				{
					if (ProgressBarFilled != null)
						ProgressBarFilled.Invoke();
				}
			}
		}

		#region TreeView
		public TreeView Tree { get { return _tree; } }
		public event NodeDelegate NodeExpandClick;
		public event NodeDelegate NodeSelected;

		#endregion

		#region ListView
		public ListView Container { get { return _containerListView; } }
		public event ContentsDelegate ContentItemDoubleClick;
		public event ContentsDelegate PathChanged;
		#endregion

		#region NavigateButtons
		public bool BackButtonActivity
		{
			get { return _buttonBack.Enabled; }
			set { _buttonBack.Enabled = value; }
		}
		public bool ForwardButtonActivity
		{
			get { return _buttonForward.Enabled; }
			set { _buttonForward.Enabled = value; }
		}
		public bool UpButtonActivity
		{
			get { return _buttonUp.Enabled; }
			set { _buttonUp.Enabled = value; }
		}

		public event Action BackButtonPressed;
		public event Action ForwardButtonPressed;
		public event Action UpButtonPressed;
		#endregion

		#region FileActionButtons
		public event ItemOperatinDelegate CoppyButtonPressed;
		public event ItemOperatinDelegate CutButtonPressed;
		public event Action PasteButtonPressed;
		public event Action CancelButtonPressed;
		public event ItemOperatinDelegate DeleteButtonPressed;

		public event Action ProgressBarFilled;


		private UIFileOperationHelper _uiFileOperationHelper;
		#endregion


		public UIView()
		{
			WindowContext = SynchronizationContext.Current;

			InitializeComponent();

			_uiFileOperationHelper = new UIFileOperationHelper(this, _buttonCut, _buttonCopy, _buttonPaste,
					_buttonCancel, _buttonDelete, _labelCancel, _labelPasteTip, _labelSourceFolderTip,
					_labelDestinationFolderTip, _progressBarOperation, _labelTotalProgress);

			_tree.AfterSelect += tree_AfterSelect;

			_tree.BeforeExpand += tree_BeforeExpand;
		}



		public bool ShowYesNoMessage(string message, string windowName)
		{
			DialogResult dialogResult = MessageBox.Show(message, windowName, MessageBoxButtons.YesNo);
			if (dialogResult == DialogResult.Yes)
				return true;
			return false;
		}


		private void buttonGo_Click(object sender, EventArgs e)
		{
			if (PathChanged != null)
				PathChanged.Invoke(_pathTextBox.Text);
		}


		private void tree_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			if (NodeExpandClick != null)
				NodeExpandClick.Invoke(e.Node);
		}
		private void tree_AfterSelect(object sender, TreeViewEventArgs e)
		{
			//not to change node's image on selecting
			_tree.SelectedImageIndex = e.Node.ImageIndex;

			if (NodeSelected != null)
				NodeSelected.Invoke(e.Node);
		}
		private void tree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if (NodeSelected != null)
				NodeSelected.Invoke(e.Node);
		}

		private void contentsListView_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (_containerListView.SelectedItems.Count == 1)
				if (ContentItemDoubleClick != null)
					ContentItemDoubleClick.Invoke(_containerListView.SelectedItems[0].Name);
		}

		private void _containerListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			List<string> selectedItems = new List<string>();

			for (int i = 0; i < _containerListView.SelectedItems.Count; i++)
			{
				selectedItems.Add(_containerListView.SelectedItems[i].Name);
			}

			_uiFileOperationHelper.SelectedItemList = selectedItems;
		}


		private void buttonBack_Click(object sender, EventArgs e)
		{
			if (BackButtonPressed != null)
				BackButtonPressed.Invoke();
		}
		private void buttonForward_Click(object sender, EventArgs e)
		{
			if (ForwardButtonPressed != null)
				ForwardButtonPressed.Invoke();
		}
		private void buttonUp_Click(object sender, EventArgs e)
		{
			if (UpButtonPressed != null)
				UpButtonPressed.Invoke();
		}



		private void _buttonCut_Click(object sender, EventArgs e)
		{
			if (CutButtonPressed != null)
				CutButtonPressed.Invoke(_uiFileOperationHelper.SelectedItemList);
		}
		private void _buttonCopy_Click(object sender, EventArgs e)
		{
			if (CoppyButtonPressed != null)
				CoppyButtonPressed.Invoke(_uiFileOperationHelper.SelectedItemList);
		}
		private void _buttonPaste_Click(object sender, EventArgs e)
		{
			if (PasteButtonPressed != null)
				PasteButtonPressed.Invoke();
		}
		private void _buttonCancel_Click(object sender, EventArgs e)
		{
			if (CancelButtonPressed != null)
				CancelButtonPressed.Invoke();
		}
		private void _buttonDelete_Click(object sender, EventArgs e)
		{
			if (ShowYesNoMessage("Do you realy want to delete these files?", "Confirm"))
			{
				if (DeleteButtonPressed != null)
					DeleteButtonPressed.Invoke(_uiFileOperationHelper.SelectedItemList);
			}
		}



		private void buttonSaveXmlReport_Click(object sender, EventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();

			saveFileDialog.Filter = "XML files(.xml)|*.xml";
			saveFileDialog.FilterIndex = 1;

			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				if (SaveReportButtonPressed != null)
					SaveReportButtonPressed.Invoke(PathString, saveFileDialog.FileName, _checkBoxBustSubfolders.Checked);
			}
		}
	}
}
