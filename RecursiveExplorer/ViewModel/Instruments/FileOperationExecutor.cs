﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using FileSystemOperator;

namespace RecursiveExplorer.ViewModel.Instruments
{
	public enum FileOperation
	{
		Cancel, Move, Copy, Delete
	}

	/// <summary>
	/// Process files operations in background.
	/// Updates operation progress.
	/// </summary>
	public class FileOperationExecutor
	{
		public string DestinationFolderPath { get; set; }

		//selected files and folders
		public List<string> OperatedItems;

		public event Action NeedUpdateView;

		public FileOperation OperationType = FileOperation.Cancel;

		//handle all operation in the background
		public BackgroundWorker _worker = new BackgroundWorker();

		//show confirm message to user and take yes/no answer
		private Func<string, bool> RewriteConfirmator;
		private IFSOperator _fsOperator;

		//while-operation variables
		public bool CancelOperation = false;
		private long _totalItemsSize;
		private long _processedSize;
		private long ProcessedSize
		{
			get { return _processedSize; }
			set
			{
				_processedSize = value;
				//avoid zero dividing
				if (_totalItemsSize == 0)
					_totalItemsSize = 1;

				int percents = (int)(100 * _processedSize / _totalItemsSize);
				_worker.ReportProgress(Convert.ToInt32(percents));
			}
		}



		public FileOperationExecutor(IFSOperator fsOperator, Func<string, bool> rewriteConfirmator, ProgressChangedEventHandler persentageChangedHandler)
		{
			_fsOperator = fsOperator;
			RewriteConfirmator = rewriteConfirmator;
			_worker.DoWork += delegate { Executing(); };
			_worker.WorkerReportsProgress = true;
			_worker.ProgressChanged += persentageChangedHandler;
		}


		private long GetItemSize(string item)
		{
			if (_fsOperator.IsItFolder(item))
				return _fsOperator.GetFolderSize(item, true);
			else
				return _fsOperator.GetFile(item).Length;
		}
		private long GetAllItemsSize()
		{
			long totalSize = 0;
			foreach (string item in OperatedItems)
			{
				if (_fsOperator.IsItFolder(item))
					totalSize += _fsOperator.GetFolderSize(item, true);
				else
					totalSize += _fsOperator.GetFile(item).Length;
			}
			return totalSize;
		}


		private void MoveFolder(string folderPath, string destinationFolderPath)
		{
			string folderToCopyPath = Path.Combine(destinationFolderPath, _fsOperator.GetFolder(folderPath).Name);

			Directory.CreateDirectory(folderToCopyPath);

			foreach (FileInfo file in _fsOperator.GetFolderFiles(folderPath))
			{
				MoveFile(file.FullName, folderToCopyPath);
			}

			foreach (DirectoryInfo folder in _fsOperator.GetFolderSubFolders(folderPath))
			{
				MoveFolder(folder.FullName, folderToCopyPath);
			}
			Directory.Delete(folderPath);
		}
		private void CopyFolder(string folderPath, string destinationFolderPath)
		{
			string folderToCopyPath = Path.Combine(destinationFolderPath, _fsOperator.GetFolder(folderPath).Name);

			Directory.CreateDirectory(folderToCopyPath);

			foreach (FileInfo file in _fsOperator.GetFolderFiles(folderPath))
			{
				CopyFile(file.FullName, folderToCopyPath);
			}

			foreach (DirectoryInfo folder in _fsOperator.GetFolderSubFolders(folderPath))
			{
				CopyFolder(folder.FullName, folderToCopyPath);
			}
		}

		private void MoveFile(string filePath, string destinationFolderPath)
		{
			if (CopyFile(filePath, destinationFolderPath))
				_fsOperator.DeleteFile(filePath);
		}
		private bool CopyFile(string filePath, string destinationFolderPath)
		{
			string destinationFileName = Path.Combine(destinationFolderPath, Path.GetFileName(filePath));

			bool overWriteChoice = true;

			if (_fsOperator.IsThereFileWithSimilarName(filePath, destinationFolderPath))
				overWriteChoice = RewriteConfirmator.Invoke(Path.GetFileName(filePath));

			if (IsFileLarge(filePath))
				CopyLargeFile(filePath, destinationFileName, overWriteChoice);
			else
			{
				_fsOperator.CopyFile(filePath, destinationFileName, overWriteChoice);
				ProcessedSize += GetItemSize(filePath);
			}

			return overWriteChoice;
		}

		private bool IsFileLarge(string filePath)
		{
			if (GetItemSize(filePath) > 10000000)// >10mb
				return true;
			return false;

		}
		private void CopyLargeFile(string filePath, string destinationFilePath, bool rewriteOption)
		{
			try
			{
				if (!rewriteOption)
					return;

				CancelOperation = false;

				byte[] buffer = new byte[1024 * 1024]; // 1MB buffer

				using (FileStream source = new FileStream(filePath, FileMode.Open, FileAccess.Read))
				{
					long fileLength = source.Length;

					if (File.Exists(destinationFilePath))
						File.Delete(destinationFilePath);

					using (FileStream dest = new FileStream(destinationFilePath, FileMode.CreateNew, FileAccess.Write))
					{
						if (NeedUpdateView != null)
							NeedUpdateView.Invoke();

						int currentBlockSize = 0;

						while ((currentBlockSize = source.Read(buffer, 0, buffer.Length)) > 0)
						{
							ProcessedSize += currentBlockSize;

							dest.Write(buffer, 0, currentBlockSize);

							if (CancelOperation == true)
							{
								dest.Close();
								File.Delete(destinationFilePath);
								break;
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}



		public void StartOperation()
		{
			_worker.RunWorkerAsync();
		}


		private void Executing()
		{
			_totalItemsSize = GetAllItemsSize();
			ProcessedSize = 0;
			CancelOperation = false;

			foreach (string itemPath in OperatedItems)
			{
				#region SwitchOperation
				switch (OperationType)
				{
					case FileOperation.Delete:
						ProcessedSize += GetItemSize(itemPath);
						if (_fsOperator.IsItFolder(itemPath))
							_fsOperator.DeleteFolder(itemPath);
						else
							_fsOperator.DeleteFile(itemPath);
						if (NeedUpdateView != null)
							NeedUpdateView.Invoke();
						break;


					case FileOperation.Move:
						if (_fsOperator.IsItFolder(itemPath))
						{
							_fsOperator.CloseAllFilles(itemPath, true);
							MoveFolder(itemPath, DestinationFolderPath);
						}
						else
							MoveFile(itemPath, DestinationFolderPath);
						if (NeedUpdateView != null)
							NeedUpdateView.Invoke();
						break;


					case FileOperation.Copy:
						if (_fsOperator.IsItFolder(itemPath))
						{
							_fsOperator.CloseAllFilles(itemPath, true);
							CopyFolder(itemPath, DestinationFolderPath);
						}
						else
							CopyFile(itemPath, DestinationFolderPath);
						if (NeedUpdateView != null)
							NeedUpdateView.Invoke();
						break;


					case FileOperation.Cancel:
						if (NeedUpdateView != null)
							NeedUpdateView.Invoke();
						break;
				}

				#endregion
			}
			_worker.ReportProgress(100);

			OperationType = FileOperation.Cancel;

			if (NeedUpdateView != null)
				NeedUpdateView.Invoke();
		}
	}
}

