﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using FileSystemOperator;

namespace RecursiveExplorer.ViewModel.Instruments
{

	public class ContentsCollection
	{
		public DirectoryInfo[] Folders;
		public FileInfo[] Files;
		public List<Icon> FilesIconList;
	}

	/// <summary>
	/// ListView loader
	/// </summary>
	public class FolderContentsBuilder
	{
		private readonly IFSOperator _fsOperator;

		public FolderContentsBuilder(IFSOperator fsOperator)
		{
			_fsOperator = fsOperator;
		}

		public ContentsCollection GetFolderContent(string folderPath)
		{
			if (Directory.Exists(folderPath))
			{
				ContentsCollection result = new ContentsCollection();

				result.FilesIconList = new List<Icon>();
				result.Folders = _fsOperator.GetFolderSubFolders(folderPath);
				result.Files = _fsOperator.GetFolderFiles(folderPath);

				foreach (FileInfo file in result.Files)
				{
					result.FilesIconList.Add(Icon.ExtractAssociatedIcon(file.FullName));
				}
				return result;
			}

			return new ContentsCollection()
			{
				Folders = new DirectoryInfo[0],
				Files = new FileInfo[0],
				FilesIconList = new List<Icon>()
			};
		}

		public void OpenFileLikeSystem(string path)
		{
			System.Diagnostics.Process.Start(path);
		}

		public bool IsItFolder(string path)
		{
			return _fsOperator.IsItFolder(path);
		}
	}
}