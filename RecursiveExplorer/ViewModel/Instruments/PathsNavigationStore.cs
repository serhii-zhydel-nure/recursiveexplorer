﻿using System.Collections.Generic;
using System.IO;

namespace RecursiveExplorer.ViewModel.Instruments
{
    public class PathsNavigationStore
    {
        public bool CanGetBack { get; private set; }
        public bool CanGoForward { get; private set; }
        public bool CanGetUp { get; private set; }

        public string CurrentPath { get; private set; }

        private Stack<string> _forward = new Stack<string>();
        private Stack<string> _back = new Stack<string>();


        public PathsNavigationStore(string currentPath)
        {
            CurrentPath = currentPath;
            CanGetBack = false;
            CanGoForward = false;
            CanGetUp = false;
            UpdateBools();
        }


        public void GoDeeper(string path)
        {
            _back.Push(CurrentPath);
            CurrentPath = path;

            //erase forward
            if (_forward.Count > 0)
                _forward = new Stack<string>();

            UpdateBools();

        }

        public string GetBack()
        {
            string backPath = _back.Pop();
            _forward.Push(CurrentPath);
            CurrentPath = backPath;
            UpdateBools();
            return backPath;
        }

        public string GoForward()
        {
            string forwardPath = _forward.Pop();
            _back.Push(CurrentPath);
            CurrentPath = forwardPath;
            UpdateBools();
            return forwardPath;
        }

        public string GetUp()
        {
            string upPath = Directory.GetParent(CurrentPath).FullName;
            
            _back.Push(CurrentPath);
            CurrentPath = upPath;
            UpdateBools();
            return upPath;
        }

        private void UpdateBools()
        {
            if (_back.Count > 0)
                CanGetBack = true;
            else
                CanGetBack = false;

            if (_forward.Count > 0)
                CanGoForward = true;
            else
                CanGoForward = false;

            if (Directory.GetParent(CurrentPath)!=null)
                CanGetUp = true;
            else
                CanGetUp = false;
        }

    }
}