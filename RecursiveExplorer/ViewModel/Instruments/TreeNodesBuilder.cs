﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using FileSystemOperator;

namespace RecursiveExplorer.ViewModel.Instruments
{
    /// <summary>
    /// TreeView loader
    /// </summary>
    public class TreeNodesBuilder
    {
        private readonly IFSOperator _fsOperator;

        public TreeNodesBuilder(IFSOperator fsOperator)
        {
            _fsOperator = fsOperator;
        }

        public TreeNode[] GetDrivesNode()
        {
            List<TreeNode> drivesNodes = new List<TreeNode>();

            foreach (DriveInfo drive in _fsOperator.GetDrives())
            {
                TreeNode driveNode = new TreeNode(drive.Name);
                driveNode.Name = drive.Name;

                #region SetIcon
                switch (drive.DriveType)
                {
                    case DriveType.Fixed:
                        driveNode.ImageIndex = 1;
                        break;

                    case DriveType.Removable:
                        driveNode.ImageIndex = 2;
                        break;

                    case DriveType.CDRom:
                        driveNode.ImageIndex = 3;
                        break;

                    default:
                        driveNode.ImageIndex = 3;
                        break;
                }
                #endregion

                //add sufolders
                if (drive.IsReady)
                    driveNode.Nodes.AddRange(GetSubFoldersNodes(drive.ToString()));

                drivesNodes.Add(driveNode);
            }

            return drivesNodes.ToArray();
        }

        public TreeNode[] GetSubFoldersNodes(string path)
        {
            DirectoryInfo[] subFolders = _fsOperator.GetFolderSubFolders(path);

            TreeNode[] result = subFolders.Select(folder => new TreeNode(folder.ToString())).ToArray();

            for (int i = 0; i < result.Length; i++)
            {
                result[i].Name = subFolders[i].FullName;
                result[i].ImageIndex = 4;
            }                              

            return result;
        }
    }
}