﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using BaseThreads;
using FileSystemOperator;
using RecursiveExplorer.Properties;
using RecursiveExplorer.View;
using RecursiveExplorer.ViewModel.Instruments;
using XMLWriter;

namespace RecursiveExplorer.ViewModel
{
	/// <summary>
	/// Main window handler.
	/// Responds on user actions in window
	/// </summary>
	public class ViewHandler
	{
		//processing view-methods
		private readonly SynchronizationContext _context;
		private readonly UIView _window;

		//looped background worker for processing this-class's methods
		private readonly CyclicalBackgroundMethodProcessor _actionsProcessor;

		#region Instrument instances
		private IFSOperator _fsOperator;
		private PathsNavigationStore _pathsNavigationStore;
		private readonly FileOperationExecutor _fileOperationExecutor;
		private readonly TreeNodesBuilder _treeNodesBuilder;
		private readonly FolderContentsBuilder _folderContentsBuilder;
		#endregion

		public ViewHandler(UIView window, IFSOperator fsOperator)
		{
			//launching of a worker
			_actionsProcessor = new CyclicalBackgroundMethodProcessor();

			//init view
			_context = window.WindowContext;
			_window = window;

			Subscription();

			//init instruments
			_fsOperator = fsOperator;
			_treeNodesBuilder = new TreeNodesBuilder(fsOperator);
			_folderContentsBuilder = new FolderContentsBuilder(fsOperator);
			_fileOperationExecutor = new FileOperationExecutor(fsOperator, ConfirmOverwrite, ChangeProgress);
			_fileOperationExecutor.NeedUpdateView += UpdateView;

			_pathsNavigationStore = new PathsNavigationStore(Environment.CurrentDirectory);
			_fileOperationExecutor.DestinationFolderPath = _pathsNavigationStore.CurrentPath;
			ChangePathString(_pathsNavigationStore.CurrentPath);

			InitTree(window.Tree);
		}

		private void Subscription()
		{
			_window.PathChanged += GoPath;

			_window.NodeSelected += LoadNodeFolderContents;
			_window.NodeExpandClick += LoadSubNodes;
			_window.ContentItemDoubleClick += OpenSelectedFileOrFolder;

			_window.BackButtonPressed += GetBack;
			_window.ForwardButtonPressed += GoForward;
			_window.UpButtonPressed += GetUp;

			_window.CutButtonPressed += CutItems;
			_window.CoppyButtonPressed += CopyItems;
			_window.PasteButtonPressed += PasteItems;
			_window.CancelButtonPressed += CancelOperation;
			_window.DeleteButtonPressed += DeleteItems;

			_window.SaveReportButtonPressed += SaveXmlFolderReport;
		}

		#region DelegateInvocation
		/// <summary>
		/// Invoke method like view-thread (thread 1)
		/// </summary>
		/// <param name="method"></param>
		private void CallMethodLikeView(Action method)
		{
			_context.Post((obj) =>
			{
				method.Invoke();
			}, null);
		}

		/// <summary>
		/// Delegate method to background thread (thread 2)
		/// </summary>
		/// <param name="method"></param>
		private void CallMethodLikeBackgroundThread(Action method)
		{
			//method.Invoke();   //for single-thread mode launching
			_actionsProcessor.AddMethodToProcessing(method.Invoke); //for second-thread mode launching
		}
		#endregion

		#region TreeHandeling
		private void InitTree(TreeView tree)
		{
			CallMethodLikeBackgroundThread(() =>
			{
							//set icons for nodes
							ImageList nodesIconList = new ImageList();
				nodesIconList.ColorDepth = ColorDepth.Depth32Bit;
				nodesIconList.ImageSize = new System.Drawing.Size(20, 20);

				nodesIconList.Images.Add(Resources.computer);
				nodesIconList.Images.Add(Resources.hdd);
				nodesIconList.Images.Add(Resources.flash);
				nodesIconList.Images.Add(Resources.disk);
				nodesIconList.Images.Add(Resources.folder);
				nodesIconList.Images.Add(Resources.file);

							//load first nodes
							TreeNode[] nodes = _treeNodesBuilder.GetDrivesNode();

				CallMethodLikeView(() =>
							{
						tree.ImageList = nodesIconList;

						tree.Nodes.AddRange(nodes);
					});
			});
		}

		private void LoadSubNodes(TreeNode node)
		{
			CallMethodLikeBackgroundThread(() =>
			{
				foreach (TreeNode subNode in node.Nodes)
				{
					TreeNode[] nodes = _treeNodesBuilder.GetSubFoldersNodes(subNode.Name);

					CallMethodLikeView(() =>
								{
								subNode.Nodes.AddRange(nodes);
							});
				}
			});
		}

		private void LoadNodeFolderContents(TreeNode node)
		{
			CallMethodLikeBackgroundThread(() =>
			{
				_pathsNavigationStore = new PathsNavigationStore(node.Name);
				ChangePathString(_pathsNavigationStore.CurrentPath);
			});
		}
		#endregion

		#region ContentsHandeling
		private void ShowFolderContent(string path)
		{
			CallMethodLikeBackgroundThread(() =>
			{
				ContentsCollection collection = _folderContentsBuilder.GetFolderContent(path);

				ImageList icons = new ImageList();
				icons.ColorDepth = ColorDepth.Depth32Bit;
				icons.ImageSize = new System.Drawing.Size(32, 32);

				CallMethodLikeView(() =>
							 {
						_window.Container.Items.Clear();
						_window.Container.LargeImageList = icons;

						icons.Images.Add(Resources.folder);

						foreach (DirectoryInfo folder in collection.Folders)
						{
							ListViewItem item = new ListViewItem(folder.Name, icons.Images.Count - 1);
							item.Name = folder.FullName;
							_window.Container.Items.Add(item);
						}

						for (int i = 0; i < collection.Files.Length; i++)
						{
							icons.Images.Add(collection.FilesIconList[i]);
							ListViewItem item = new ListViewItem(collection.Files[i].Name, icons.Images.Count - 1);
							item.Name = collection.Files[i].FullName;
							_window.Container.Items.Add(item);
						}
					});
			});
		}

		private void OpenSelectedFileOrFolder(string path)
		{
			CallMethodLikeBackgroundThread(() =>
			{
				bool isFolder = _folderContentsBuilder.IsItFolder(path);

				if (isFolder)
				{
					ShowFolderContent(path);
					_pathsNavigationStore.GoDeeper(path);
					ChangePathString(path);
				}
				else
				{
					_folderContentsBuilder.OpenFileLikeSystem(path);
				}
			});

		}
		#endregion

		#region NavigationButton

		private void GetBack()
		{
			string path = _pathsNavigationStore.GetBack();
			UpdatePathString(path);
		}

		private void GoForward()
		{
			string path = _pathsNavigationStore.GoForward();
			UpdatePathString(path);
		}

		private void GetUp()
		{
			string path = _pathsNavigationStore.GetUp();
			UpdatePathString(path);
		}

		private void UpdatePathString(string path)
		{
			ChangePathString(path);
			UpdateButtonsState();
		}

		private void UpdateButtonsState()
		{
			_window.BackButtonActivity = _pathsNavigationStore.CanGetBack;
			_window.ForwardButtonActivity = _pathsNavigationStore.CanGoForward;
			_window.UpButtonActivity = _pathsNavigationStore.CanGetUp;
		}

		#endregion

		#region FileOperations
		private void CutItems(List<string> items)
		{
			_fileOperationExecutor.OperatedItems = items;
			_fileOperationExecutor.OperationType = FileOperation.Move;
		}

		private void CopyItems(List<string> items)
		{
			_fileOperationExecutor.OperatedItems = items;
			_fileOperationExecutor.OperationType = FileOperation.Copy;
		}

		private void PasteItems()
		{
			_fileOperationExecutor.DestinationFolderPath = _window.PathString;
			_fileOperationExecutor.StartOperation();
		}

		private bool ConfirmOverwrite(string fileName)
		{
			return _window.ShowYesNoMessage(String.Format("Folder already containts file with the same name: \"{0}\". {1} Overwrite this file?", fileName, Environment.NewLine),
					 "Warning!");
		}

		private void CancelOperation()
		{
			_fileOperationExecutor.OperationType = FileOperation.Cancel;
			_fileOperationExecutor.CancelOperation = true;
		}

		private void DeleteItems(List<string> items)
		{
			_fileOperationExecutor.DestinationFolderPath = _window.PathString;
			_fileOperationExecutor.OperatedItems = items;
			_fileOperationExecutor.OperationType = FileOperation.Delete;
			_fileOperationExecutor.StartOperation();
		}

		private void ChangeProgress(object sender, ProgressChangedEventArgs e)
		{
			CallMethodLikeView(() =>
			{
				_window.ProgressOperation = e.ProgressPercentage;
			});
		}

		private void UpdateView()
		{
			CallMethodLikeView(() =>
			{
				if (_window.PathString == _fileOperationExecutor.DestinationFolderPath)
					ShowFolderContent(_fileOperationExecutor.DestinationFolderPath);
			});
		}
		#endregion

		#region StringPathHandeling

		private void ChangePathString(string path)
		{
			CallMethodLikeBackgroundThread(() =>
			{
				ShowFolderContent(_pathsNavigationStore.CurrentPath);

				CallMethodLikeView(() =>
							{
						_window.PathString = _pathsNavigationStore.CurrentPath;
						UpdateButtonsState();
					});
			});
		}


		private void GoPath(string path)
		{
			CallMethodLikeBackgroundThread(() =>
			{
				if (Directory.Exists(path) && _pathsNavigationStore.CurrentPath != path)
				{
					_pathsNavigationStore.GoDeeper(path.TrimEnd(Path.DirectorySeparatorChar));
					ChangePathString(_pathsNavigationStore.CurrentPath);
				}
				CallMethodLikeView(() => UpdateButtonsState());
			});
		}

		#endregion

		private void SaveXmlFolderReport(string folderName, string fileToSave, bool bustSubfolders)
		{
			XmlFolderReportMaker.GetInstance().MakeFolderReport(folderName, fileToSave, _fsOperator, bustSubfolders);
		}
	}
}