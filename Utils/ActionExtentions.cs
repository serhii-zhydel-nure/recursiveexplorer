﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{

  public static class ActionFunkExtender
  {
    public static void InvokeChecked(this Action action)
    {
      if (action.HasSubscriptions()) action.Invoke();
    }
    public static void InvokeChecked<P>(this Action<P> action, P param)
    {
      if (action.HasSubscriptions()) action.Invoke(param);
    }

    public static R InvokeChecked<R>(this Func<R> funk)
    {
      if (funk.HasSubscriptions())
        return funk.Invoke();
      return default(R);
    }
    public static R InvokeChecked<P, R>(this Func<P, R> funk, P param)
    {
      if (funk.HasSubscriptions())
        return funk.Invoke(param);
      return default(R);
    }

    public static void Append(this Action action, Action nextAction)
    {
      action.InvokeChecked();
      nextAction.InvokeChecked();
    }
    public static void Append<P>(this Action<P> action, P param, Action nextAction)
    {
      action.InvokeChecked(param);
      nextAction.InvokeChecked();
    }
    public static void Append<P>(this Action action, Action<P> nextAction, P param)
    {
      action.InvokeChecked();
      nextAction.InvokeChecked(param);
    }
    public static void Append<P>(this Action<P> action, P param1, Action<P> nextAction, P param2)
    {
      action.InvokeChecked(param1);
      nextAction.InvokeChecked(param2);
    }

    public static bool HasSubscriptions(this Action action)
    {
      return action != null && action.GetInvocationList().Any();
    }
    public static bool HasSubscriptions<P>(this Action<P> action)
    {
      return action != null && action.GetInvocationList().Any();
    }

    public static bool HasSubscriptions<R>(this Func<R> funk)
    {
      return funk != null && funk.GetInvocationList().Any();
    }
    public static bool HasSubscriptions<P, R>(this Func<P, R> funk)
    {
      return funk != null && funk.GetInvocationList().Any();
    }
  }
}
