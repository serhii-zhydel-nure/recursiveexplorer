﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Xml.Linq;
using FileSystemOperator;

namespace XMLWriter
{
    [Serializable]
    public class SerializableFile
    {
        public string Name;
        public DateTime CreateDate;
        public DateTime ModifyDate;
        public DateTime LastAccessDate;
        public FileAttributes Attributes;
        public long Size;
        public string Owner;
        public List<FileSystemRights> CurrentUserRights;

        [NonSerialized]
        private static IFSOperator _fsOperator;

      

        public SerializableFile()
        {
            CurrentUserRights = new List<FileSystemRights>();
        }

        public SerializableFile(FileInfo file, IFSOperator fsOperator) :this()
        {
            _fsOperator = fsOperator;
            Name = file.Name;
            CreateDate = file.CreationTime;
            ModifyDate = file.LastWriteTime;
            LastAccessDate = file.LastAccessTime;
            Attributes = file.Attributes;
            Size = file.Length;
            Owner = fsOperator.GetOwner(file.FullName);
            CurrentUserRights = fsOperator.GetUserRights(file.FullName);
         }

        public XElement FileToXml()
        {
            return new XElement("File",
                new XAttribute("Name", Name),
                new XElement("Size-bytes", Size),
                new XElement("Attributes", Attributes),
                new XElement("Owner", Owner),
                new XElement("Create-date", CreateDate.ToString("dd/MM/yyyy")),
                new XElement("Modify-date", ModifyDate.ToString("dd/MM/yyyy")),
                new XElement("Last-access-date", LastAccessDate.ToString("dd/MM/yyyy")),

                new XElement("CurrentUserRights",
                    from right in CurrentUserRights
                    select new XElement("Rights", right)));
        }
    }
}