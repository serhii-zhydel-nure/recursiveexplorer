﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Xml.Linq;
using FileSystemOperator;

namespace XMLWriter
{
    public class SerializableFolder
    {
        public string Name;
        public DateTime CreateDate;
        public DateTime ModifyDate;
        public DateTime LastAccessDate;
        public FileAttributes Attributes;
        public long Size;
        public string Owner;
        public List<FileSystemRights> CurrentUserRights;
        public List<SerializableFolder> SubFolders;
        public List<SerializableFile> Files; 

        [NonSerialized]
        private static IFSOperator _fsOperator;


        public SerializableFolder()
        {
            CurrentUserRights = new List<FileSystemRights>();
            SubFolders = new List<SerializableFolder>();
            Files = new List<SerializableFile>();
        }

        //uses only by recursive constructor
        private SerializableFolder(DirectoryInfo folder, IFSOperator fsOperator) : this()
        {
            _fsOperator = fsOperator;
            Name = folder.Name;
            CreateDate = folder.CreationTime;
            ModifyDate = folder.LastWriteTime;
            LastAccessDate = folder.LastAccessTime;
            Attributes = folder.Attributes;
            Owner = fsOperator.GetOwner(folder.FullName);

            foreach (FileSystemRights rule in fsOperator.GetUserRights(folder.FullName))
            {
                CurrentUserRights.Add(rule);
            }

            Size = fsOperator.GetFolderSize(folder.FullName, false);
        }

        public SerializableFolder(DirectoryInfo folder, IFSOperator fsOperator, bool recursiveLoading) : this(folder, fsOperator)
        {
            if (recursiveLoading)
                foreach (DirectoryInfo subFolder in _fsOperator.GetFolderSubFolders(folder.FullName))
                    SubFolders.Add(new SerializableFolder(subFolder, fsOperator, recursiveLoading));
            else
                foreach (DirectoryInfo subFolder in _fsOperator.GetFolderSubFolders(folder.FullName))
                    SubFolders.Add(new SerializableFolder(subFolder, fsOperator));

            foreach (FileInfo file in folder.GetFiles())
            {
                Files.Add(new SerializableFile(file, _fsOperator));
            }

            if (recursiveLoading)
                foreach (SerializableFolder subFolder in SubFolders)
                {
                    Size += subFolder.Size;
                }
        }


        public XElement CreateFolderXmlTree()
        {
            return new XElement("Folder",
                new XAttribute("Name", Name),
                new XElement("Size-bytes", Size),
                new XElement("Attributes", Attributes),
                new XElement("Owner", Owner),
                new XElement("Create-date", CreateDate.ToString("dd/MM/yyyy")),
                new XElement("Modify-date", ModifyDate.ToString("dd/MM/yyyy")),
                new XElement("Last-access-date", LastAccessDate.ToString("dd/MM/yyyy")),

                new XElement("CurrentUserRights",
                from right in CurrentUserRights
                select new XElement("Rights", right)),

                new XElement("SubFolders",
                from subFolder in SubFolders
                select new XElement("Folder", subFolder.CreateFolderXmlTree(),
                new XAttribute("Name", subFolder.Name))),

                new XElement("Files",
                from file in Files
                select new XElement("File", file.FileToXml(),
                new XAttribute("Name", file.Name))));
        }
        
    }
}