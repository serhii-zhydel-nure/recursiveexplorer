﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.Linq;
using FileSystemOperator;
using BaseThreads;

namespace XMLWriter
{
	public class XmlFolderReportMaker
	{
		private static readonly XmlFolderReportMaker _instance = new XmlFolderReportMaker();

		protected XmlFolderReportMaker()
		{
		}

		public static XmlFolderReportMaker GetInstance()
		{
			return _instance;
		}

		public void MakeFolderReport(string folderPath, string reportFilePath, IFSOperator fsOperator, bool recursiveWrite)
		{
			List<object> parameters = new List<object>() 
			{
				folderPath ,
				reportFilePath,
				fsOperator,
				recursiveWrite
			};

			new DisposableNonBackgroundFunkProcessor(MakeReport, parameters); //making report
		}

		private object MakeReport(object obj)
		{
			List<object> parameters = obj as List<object>;
			IFSOperator fsOperator = parameters[2] as IFSOperator;

			SerializableFolder serializableFolder = new SerializableFolder(
							fsOperator.GetFolder(parameters[0] as string),
							fsOperator, (bool)parameters[3]);

			XElement fileSystemTree = serializableFolder.CreateFolderXmlTree();

			using (StreamWriter writer = new StreamWriter(parameters[1] as string))
			{
				writer.WriteLine(fileSystemTree);
			}

			return null;
		}

	}
}